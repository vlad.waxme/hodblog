
-- phpMyAdmin SQL Dump
-- version 4.0.6
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 11 2014 г., 13:48
-- Версия сервера: 5.5.33
-- Версия PHP: 5.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- База данных: `self_hodblog`
--

--
-- Дамп данных таблицы `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `title`, `alias`, `description`, `created`, `modified`) VALUES
(1, 'Cars', 'cars', '', '2014-07-11 07:29:19', '2014-07-11 07:33:53'),
(2, 'Boats', 'boats', '', '2014-07-11 07:34:08', NULL),
(3, 'bikes', 'bikes', '', '2014-07-11 07:45:11', NULL);

--
-- Дамп данных таблицы `tbl_article`
--

INSERT INTO `tbl_article` (`id`, `category_id`, `title`, `alias`, `content`, `created`, `modified`) VALUES
(1, 2, 'New Boat', 'newboat', '<p>A <b>boat</b> is a <a href="/wiki/Watercraft" title="Watercraft">watercraft</a> of any size designed to float or <a href="/wiki/Planing_(boat)" title="Planing (boat)">plane</a>, to <a href="/wiki/Boating" title="Boating">work or travel on water</a>. Small boats are typically found on inland (<a href="/wiki/Lake" title="Lake">lakes</a>) or in protected coastal areas. However, boats such as the <a href="/wiki/Whaleboat" title="Whaleboat">whaleboat</a> were designed for operation from a <a href="/wiki/Ship" title="Ship">ship</a> in an offshore environment. In <a href="/wiki/Navy" title="Navy">naval</a> terms, a boat is a vessel small enough to be carried aboard another vessel (a ship). Another less restrictive definition is a vessel that can be lifted out of the water. Some definitions do not make a distinction in size, as 1000-foot bulk freighters on the Great Lakes are called <a href="/wiki/Lake_freighter" title="Lake freighter">oreboats</a>. For reasons of <a href="/wiki/Naval_tradition" title="Naval tradition">naval tradition</a>, <a href="/wiki/Submarine" title="Submarine">submarines</a> are usually referred to as ''boats'' rather than ''<a href="/wiki/Ship" title="Ship">ships</a>'', regardless of their size.</p>', '2014-07-11 08:09:28', NULL),
(2, 1, 'New Car', 'newcar', '<p>An <b>automobile</b>, <b>autocar</b>, <b>motor car</b> or <b>car</b> is a wheeled <a href="/wiki/Motor_vehicle" title="Motor vehicle">motor vehicle</a> used for <a href="/wiki/Transportation" title="Transportation" class="mw-redirect">transporting</a> <a href="/wiki/Passenger" title="Passenger">passengers</a>, which also carries its own engine or motor. Most definitions of the term specify that automobiles are designed to run primarily on roads, to have seating for one to eight people, to typically have four wheels, and to be constructed principally for the transport of people rather than goods.<sup id="cite_ref-3" class="reference"><a href="#cite_note-3"><span>[</span>3<span>]</span></a></sup></p>\r\n<p>The year 1886 is regarded the year of birth of the modern automobile—with the <a href="/wiki/Benz_Patent-Motorwagen" title="Benz Patent-Motorwagen">Benz Patent-Motorwagen</a>, by German inventor <a href="/wiki/Karl_Benz" title="Karl Benz">Karl Benz</a>. Motorized wagons soon replaced animal-drafted <a href="/wiki/Carriage" title="Carriage">carriages</a>, especially after automobiles became affordable for many people when the <a href="/wiki/Ford_Model_T" title="Ford Model T">Ford Model T</a> was introduced in 1908.</p>', '2014-07-11 08:39:14', NULL),
(3, 1, 'New Car', 'newcar-1', '<p>An <b>automobile</b>, <b>autocar</b>, <b>motor car</b> or <b>car</b> is a wheeled <a href="/wiki/Motor_vehicle" title="Motor vehicle">motor vehicle</a> used for <a href="/wiki/Transportation" title="Transportation" class="mw-redirect">transporting</a> <a href="/wiki/Passenger" title="Passenger">passengers</a>, which also carries its own engine or motor. Most definitions of the term specify that automobiles are designed to run primarily on roads, to have seating for one to eight people, to typically have four wheels, and to be constructed principally for the transport of people rather than goods.<sup id="cite_ref-3" class="reference"><a href="#cite_note-3"><span>[</span>3<span>]</span></a></sup></p>\r\n<p>The year 1886 is regarded the year of birth of the modern automobile—with the <a href="/wiki/Benz_Patent-Motorwagen" title="Benz Patent-Motorwagen">Benz Patent-Motorwagen</a>, by German inventor <a href="/wiki/Karl_Benz" title="Karl Benz">Karl Benz</a>. Motorized wagons soon replaced animal-drafted <a href="/wiki/Carriage" title="Carriage">carriages</a>, especially after automobiles became affordable for many people when the <a href="/wiki/Ford_Model_T" title="Ford Model T">Ford Model T</a> was introduced in 1908.</p>', '2014-07-11 08:39:44', NULL),
(4, 3, 'New Bike', 'newbike', '<p><b>Bike</b> is a common <a href="/wiki/Turkish_name" title="Turkish name">Turkish given name</a>. It is an <a href="/wiki/Oghuz_languages" title="Oghuz languages">Oghuz</a> accented version of <a href="/wiki/B%C3%BCke" title="Büke">Büke</a> which is also a Turkish given name. In Turkish, "Bike" means "Queen" and/or "Woman". It also means "wise, old person"; "bride"; or "the dragon with seven heads" in an old Turkish epic. It is also the name of one of the years in the "Twelve Animal Turkish Calendar".</p>\r\n<ul>\r\n<li>Bike Baran, Turkish <a href="/wiki/Pop_music" title="Pop music">pop music</a> singer.</li>\r\n<li>Bike Kocao?lu, dean of Faculty of <a href="/wiki/Fine_Arts" title="Fine Arts" class="mw-redirect">Fine Arts</a> at <a href="/wiki/Yeditepe_University" title="Yeditepe University">Yeditepe University</a>.</li>\r\n</ul>', '2014-07-11 08:40:45', NULL),
(5, 3, 'New Bike', 'newbike-1', '<p><b>Bike</b> is a common <a href="/wiki/Turkish_name" title="Turkish name">Turkish given name</a>. It is an <a href="/wiki/Oghuz_languages" title="Oghuz languages">Oghuz</a> accented version of <a href="/wiki/B%C3%BCke" title="Büke">Büke</a> which is also a Turkish given name. In Turkish, "Bike" means "Queen" and/or "Woman". It also means "wise, old person"; "bride"; or "the dragon with seven heads" in an old Turkish epic. It is also the name of one of the years in the "Twelve Animal Turkish Calendar".</p>\r\n<ul>\r\n<li>Bike Baran, Turkish <a href="/wiki/Pop_music" title="Pop music">pop music</a> singer.</li>\r\n<li>Bike Kocao?lu, dean of Faculty of <a href="/wiki/Fine_Arts" title="Fine Arts" class="mw-redirect">Fine Arts</a> at <a href="/wiki/Yeditepe_University" title="Yeditepe University">Yeditepe University</a>.</li>\r\n</ul>', '2014-07-11 08:40:46', NULL);

